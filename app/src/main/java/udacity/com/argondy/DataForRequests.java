package udacity.com.argondy;

import java.io.Serializable;

class DataForRequests implements Serializable {

    String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
