package udacity.com.argondy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends Activity implements AdapterView.OnItemSelectedListener {
    Button msearchForEventsBtn;

    //we put the real values of this list when we use the spinners to make the-
    //search and then we  put this list on bundle and send it to SearchResultActivity
    List<DataForEvent> resultOfSearchList;
    private DataForEvent d1,d2,d3,d4,d5,d6,d7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        // Spinner element
        operateSpinners();
        addingdataToTheListOfResults();


        msearchForEventsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SearchActivity.this,SearchResultActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("search result", (ArrayList<? extends Parcelable>) resultOfSearchList);
                i.putExtras(bundle);
                startActivity(i);
            }
        });


    }

    private void addingdataToTheListOfResults() {
        resultOfSearchList = new ArrayList<>();
        d1=new DataForEvent();
        d2=new DataForEvent();
        d3=new DataForEvent();
        d4=new DataForEvent();
        d5=new DataForEvent();
        d6=new DataForEvent();
        d7=new DataForEvent();

        d1.setdescription("قامت جمعية الاورمان بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الاربعاء الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d1.setname("جمعية الاورمان");
        d1.setimage(R.drawable.argondy_logo);

        d2.setdescription("قامت جمعية مصر الخير بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الثلاثاء الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d2.setname("جمعية مصر الخير");
        d2.setimage(R.drawable.argondy_logo);

        d3.setdescription("قامت جمعية رساله بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الاثنين الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d3.setname("جمعية رساله");
        d3.setimage(R.drawable.argondy_logo);

        d4.setdescription("قامت جمعية صناع الحياه بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الاحد الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d4.setname("جمعية صناع الحياه");
        d4.setimage(R.drawable.argondy_logo);

        d4.setdescription("قامت جمعية بهيه بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d4.setname("جمعية بهيه");
        d4.setimage(R.drawable.argondy_logo);

        d5.setdescription("قامت جمعية بهيه بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d5.setname("جمعية بهيه");
        d5.setimage(R.drawable.argondy_logo);

        d6.setdescription("قامت جمعية رساله بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d6.setname("جمعية رساله");
        d6.setimage(R.drawable.argondy_logo);

        d7.setdescription("قامت جمعية مصر الخير بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d7.setname("جمعية مصر الخير");
        d7.setimage(R.drawable.argondy_logo);

        resultOfSearchList.add(d1);
        resultOfSearchList.add(d2);
        resultOfSearchList.add(d3);
        resultOfSearchList.add(d4);
        resultOfSearchList.add(d5);
        resultOfSearchList.add(d6);
        resultOfSearchList.add(d7);

    }

    private void operateSpinners() {
        Spinner spinnerforcountry = (Spinner) findViewById(R.id.spinnerForCountry);
        Spinner spinnerforname = (Spinner) findViewById(R.id.spinnerForName);
        Spinner spinnerforEventType = (Spinner) findViewById(R.id.spinnerForEvent);

        msearchForEventsBtn= (Button)  findViewById(R.id.searchForEventsBtn);

        // Spinner click listener
        spinnerforcountry.setOnItemSelectedListener(this);
        spinnerforname.setOnItemSelectedListener(this);
        spinnerforEventType.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categoriesforcountry = new ArrayList<String>();
        List<String> categoriesforname = new ArrayList<String>();
        List<String> categoriesforEventType = new ArrayList<String>();

        categoriesforcountry.add("اختيار الكل");
        categoriesforcountry.add("القاهره");
        categoriesforcountry.add("أسوان");

        categoriesforname.add("اختيار الكل");
        categoriesforname.add("رساله");
        categoriesforname.add("الاورمان");
        categoriesforname.add("صناع الحياه");
        categoriesforname.add("بهيه");
        categoriesforname.add("مصر الخير");

        categoriesforEventType.add("اختيار الكل");
        categoriesforEventType.add("مناسبة فرح");
        categoriesforEventType.add("مناسبة جنازه");
        categoriesforEventType.add("افتتاح");
        categoriesforEventType.add("أخرى");

        // Creating adapter for spinnerforcountry

        // categoriesforcountry it is the list that will abears in the frist list
        ArrayAdapter<String> dataAdapterforcountry = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categoriesforcountry);
        ArrayAdapter<String> dataAdapterforname = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categoriesforname);
        ArrayAdapter<String> dataAdapterforEventType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categoriesforEventType);

        // Drop down layout style - list view with radio button
        dataAdapterforcountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapterforname.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapterforEventType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinnerforcountry
        spinnerforcountry.setAdapter(dataAdapterforcountry);
        spinnerforname.setAdapter(dataAdapterforname);
        spinnerforEventType.setAdapter(dataAdapterforEventType);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }
}
