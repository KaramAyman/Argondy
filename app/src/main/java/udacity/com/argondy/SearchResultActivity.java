package udacity.com.argondy;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity implements AddapterViewForEvents.AddapterOnClickListener {
    ArrayList<DataForEvent> resultOfSearchedList;
    AddapterViewForEvents addapterViewForSearchResult;
    public RecyclerView RecyclerViewForSearchResult;
    ProgressBar ProgressBarForSearchResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        resultOfSearchedList=new ArrayList<>();
        RecyclerViewForSearchResult = findViewById(R.id.recyclerviewForSearchResultEvents);
        LinearLayoutManager linearVertical = new LinearLayoutManager(this);
        linearVertical.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerViewForSearchResult.setLayoutManager(linearVertical);
        addapterViewForSearchResult = new AddapterViewForEvents(this ,this);
        RecyclerViewForSearchResult.setAdapter(addapterViewForSearchResult);
        ProgressBarForSearchResult = findViewById(R.id.main_progressBarForResultSearchEvents);
        ProgressBarForSearchResult.setVisibility(View.GONE);


        Intent data =getIntent();
        Bundle bundle = data.getExtras();
        if (bundle != null) {
            resultOfSearchedList= bundle.getParcelableArrayList("search result");
            if (resultOfSearchedList != null)
            addapterViewForSearchResult.setEvents(resultOfSearchedList);
        }
    }

    @Override
    public void OnClick(DataForEvent mClickedEvent) {
        Toast toast=Toast.makeText(getApplicationContext(),"Hello Java t point",Toast.LENGTH_SHORT);
        toast.show();
    }
}
