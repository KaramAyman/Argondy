package udacity.com.argondy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    EditText username,password;
    Button signin;

    String usernamestring,passwordString;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username =(EditText) findViewById(R.id.editTextForEmailLogin);
        password =(EditText) findViewById(R.id.editTextForPasswordLogin);
        signin =(Button) findViewById(R.id.buttonForSignIn);

        usernamestring=username.getText().toString();
        passwordString=password.getText().toString();

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(LoginActivity.this,HomeActivity.class);
                startActivity(i);
            }
        });
    }
}