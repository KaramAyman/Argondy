package udacity.com.argondy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class DayEventsActivity extends AppCompatActivity implements AddapterViewForEvents.AddapterOnClickListener {
    List<DataForEvent> listOfEventsInThatDay;
    public RecyclerView RecyclerViewForThatDayEvents;
    ProgressBar ProgressBarForThatDayEvents;
    AddapterViewForEvents addapterViewForThatDayEvents;
   private DataForEvent d1,d2,d3,d4,d5,d6,d7;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_notification:
                intent = new Intent(DayEventsActivity.this, NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.action_search:
                intent = new Intent(DayEventsActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_events);

        listOfEventsInThatDay = new ArrayList<>();
        RecyclerViewForThatDayEvents = findViewById(R.id.recyclerviewForDayEvents);
        LinearLayoutManager linearVertical = new LinearLayoutManager(this);
        linearVertical.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerViewForThatDayEvents.setLayoutManager(linearVertical);
        addapterViewForThatDayEvents = new AddapterViewForEvents(this ,this);
        RecyclerViewForThatDayEvents.setAdapter(addapterViewForThatDayEvents);
        ProgressBarForThatDayEvents = findViewById(R.id.main_progressBarForDayEvents);
        ProgressBarForThatDayEvents.setVisibility(View.GONE);


        addingTheEvents();
    }

    private void addingTheEvents() {

        d1=new DataForEvent();
        d2=new DataForEvent();
        d3=new DataForEvent();
        d4=new DataForEvent();
        d5=new DataForEvent();
        d6=new DataForEvent();
        d7=new DataForEvent();

        d1.setdescription("قامت جمعية الاورمان بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الاربعاء الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d1.setname("جمعية الاورمان");
        d1.setimage(R.drawable.image_for_orgnization);

        d2.setdescription("قامت جمعية مصر الخير بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الثلاثاء الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d2.setname("جمعية مصر الخير");
        d2.setimage(R.drawable.image_for_orgnization);

        d3.setdescription("قامت جمعية رساله بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الاثنين الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d3.setname("جمعية رساله");
        d3.setimage(R.drawable.image_for_orgnization);

        d4.setdescription("قامت جمعية صناع الحياه بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم الاحد الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d4.setname("جمعية صناع الحياه");
        d4.setimage(R.drawable.image_for_orgnization);

        d4.setdescription("قامت جمعية بهيه بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d4.setname("جمعية بهيه");
        d4.setimage(R.drawable.image_for_orgnization);

        d5.setdescription("قامت جمعية بهيه بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d5.setname("جمعية بهيه");
        d5.setimage(R.drawable.image_for_orgnization);

        d6.setdescription("قامت جمعية رساله بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d6.setname("جمعية رساله");
        d6.setimage(R.drawable.image_for_orgnization);

        d7.setdescription("قامت جمعية مصر الخير بعمل مناسبه فرح حيث تكون تلك المناسبه في يوم السبت الموافق 19/8/2017 وذلك في تمام الساعه الثامنه مساء ");
        d7.setname("جمعية مصر الخير");
        d7.setimage(R.drawable.image_for_orgnization);


        listOfEventsInThatDay.add(d1);
        listOfEventsInThatDay.add(d2);
        listOfEventsInThatDay.add(d3);
        listOfEventsInThatDay.add(d4);
        listOfEventsInThatDay.add(d5);
        listOfEventsInThatDay.add(d6);
        listOfEventsInThatDay.add(d7);



        addapterViewForThatDayEvents.setEvents(listOfEventsInThatDay);
    }

    @Override
    public void OnClick(DataForEvent mClickedEvent) {
        Toast toast=Toast.makeText(getApplicationContext(),"Hello Javatpoint",Toast.LENGTH_SHORT);
        toast.show();
    }
}
