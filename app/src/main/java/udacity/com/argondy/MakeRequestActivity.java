package udacity.com.argondy;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
public class MakeRequestActivity extends AppCompatActivity {

    RadioGroup groupeOfRequests;
    EditText eTForOtherRequest;
    Button sendRequestButton;
    String theOtherRequestString;
    DataForRequests dataForRequests;
    List<RequstType> listOfRequstTypes;
    RequstType dataforListOfRequstType1, dataforListOfRequstType2, dataforListOfRequstType3, dataforListOfRequstType4, dataforListOfRequstType5;

    public String theRequest;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_notification:
                intent = new Intent(MakeRequestActivity.this, NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.action_search:
                intent = new Intent(MakeRequestActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_request);


        dataForRequests = new DataForRequests();
        listOfRequstTypes = new ArrayList<>();
        dataforListOfRequstType1 = new RequstType();
        dataforListOfRequstType2 = new RequstType();
        dataforListOfRequstType3 = new RequstType();
        dataforListOfRequstType4 = new RequstType();
        dataforListOfRequstType5 = new RequstType();

        dataforListOfRequstType1.setRequestType("شرا شقه");
        dataforListOfRequstType2.setRequestType("حجز قاعة أفراح");
        dataforListOfRequstType3.setRequestType("طلب أكل");
        dataforListOfRequstType4.setRequestType("حجز مناسبه");
        dataforListOfRequstType5.setRequestType("اجار شقه");

        listOfRequstTypes.add(dataforListOfRequstType1);
        listOfRequstTypes.add(dataforListOfRequstType2);
        listOfRequstTypes.add(dataforListOfRequstType3);
        listOfRequstTypes.add(dataforListOfRequstType4);
        listOfRequstTypes.add(dataforListOfRequstType5);

        groupeOfRequests = findViewById(R.id.radioGroupRequests);
        groupeOfRequests.setOrientation(LinearLayout.VERTICAL);
        groupeOfRequests.setHorizontalGravity(20);

        eTForOtherRequest = findViewById(R.id.editTextForOtherRequests);
        sendRequestButton = findViewById(R.id.buttonForSendRequest);



        addRBs();
        findViewById(R.id.makeRequest_clearSelection_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupeOfRequests.removeAllViews();
                addRBs();
                eTForOtherRequest.setText("");
                eTForOtherRequest.setEnabled(true);
            }
        });
        groupeOfRequests.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId<listOfRequstTypes.size())
                theRequest = listOfRequstTypes.get(checkedId).getRequestType();
                eTForOtherRequest.setEnabled(false);
                Toast.makeText(MakeRequestActivity.this, theRequest, Toast.LENGTH_SHORT).show();
            }
        });
        sendRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!eTForOtherRequest.isEnabled()) {
                    dataForRequests.setRequest(theRequest);
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("one request",  dataForRequests);
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else{
                    if (eTForOtherRequest.getText().toString().length() > 0 ) {
                        theOtherRequestString = eTForOtherRequest.getText().toString();
                        dataForRequests.setRequest(theOtherRequestString);
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("one request",  dataForRequests);
                        intent.putExtras(bundle);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }else{
                        Toast.makeText(MakeRequestActivity.this, "بالرجاء اختيار من الطلبات المتاحه او كتابة طلبك أعلاه", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    void addRBs(){
        for (int i = 0; i < listOfRequstTypes.size(); i++) {
            RadioButton rbn = new RadioButton(this);
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                    RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            rbn.setLayoutParams(params);
            rbn.setId(i);
            rbn.setText(listOfRequstTypes.get(i).getRequestType());
            rbn.setTextSize(20);
            groupeOfRequests.addView(rbn);
        }
    }


}