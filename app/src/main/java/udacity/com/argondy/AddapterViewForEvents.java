package udacity.com.argondy;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class AddapterViewForEvents extends RecyclerView.Adapter<AddapterViewForEvents.ForecastAdapterViewHolder> {

    private List<DataForEvent> eventsThatWillViewed;
    Context mContext;
    private final AddapterOnClickListener mClickListener;

    public interface AddapterOnClickListener {
        void OnClick(DataForEvent mClickedWeek);
    }

    public AddapterViewForEvents(Context context, AddapterOnClickListener clickListener) {
        eventsThatWillViewed = new ArrayList<>();
        mContext = context;
        mClickListener = clickListener;
    }

    public class ForecastAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView descriptionEfEvent, nameOfVillage;
        ImageView villageImage;

        public ForecastAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            descriptionEfEvent = itemView.findViewById(R.id.Description_of_event);
            nameOfVillage = itemView.findViewById(R.id.name_of_village);
            villageImage = itemView.findViewById(R.id.imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int addapterPosition = getAdapterPosition();
                    DataForEvent ClickedName = eventsThatWillViewed.get(addapterPosition);
                    mClickListener.OnClick(ClickedName);
                }
            });
        }
    }


    public void setEvents(List<DataForEvent> mEventsThatWillViewed) {
        eventsThatWillViewed.clear();
        eventsThatWillViewed.addAll(mEventsThatWillViewed);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ForecastAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int layoutIdForBakingItem = R.layout.event_item;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(layoutIdForBakingItem, viewGroup, false);
        return new ForecastAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddapterViewForEvents.ForecastAdapterViewHolder forecastAdapterViewHolder, int i) {
        // You Use This To Full The RecyclerView Items
        String mDescription = eventsThatWillViewed.get(i).getdescription();
        String mName = eventsThatWillViewed.get(i).getname();
        Integer mImage = eventsThatWillViewed.get(i).getimage();
        forecastAdapterViewHolder.descriptionEfEvent.setText(mDescription);
        forecastAdapterViewHolder.nameOfVillage.setText(mName);
        forecastAdapterViewHolder.villageImage.setImageResource(mImage);
    }
    @Override
    public int getItemCount() {
        return eventsThatWillViewed.size();
    }
}