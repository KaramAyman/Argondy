package udacity.com.argondy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RequestsActivity extends AppCompatActivity implements AddapterViewForRequests.AddapterOnClickListener {

    List<DataForRequests> listOfRequestsForRequests;
    public RecyclerView mRecyclerViewForRequests;
    ProgressBar mProgressBarForRequests;
    AddapterViewForRequests addapterViewForRequests;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_notification:
                intent = new Intent(RequestsActivity.this, NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.action_search:
                intent = new Intent(RequestsActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
       /* Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        listOfRequestsForRequests = new ArrayList<>();
        mRecyclerViewForRequests = findViewById(R.id.recyclerviewForRequests);
        LinearLayoutManager linearVertical = new LinearLayoutManager(this);
        linearVertical.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerViewForRequests.setLayoutManager(linearVertical);
        addapterViewForRequests = new AddapterViewForRequests(this, this);
        mRecyclerViewForRequests.setAdapter(addapterViewForRequests);
        mProgressBarForRequests = findViewById(R.id.main_progressBarForRequests);
        mProgressBarForRequests.setVisibility(View.GONE);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestsActivity.this, MakeRequestActivity.class);
                startActivityForResult(intent, 1001);
            }
        });
    }

    @Override
    public void OnClick(DataForRequests mClickedRequest) {
        Toast toast = Toast.makeText(getApplicationContext(), mClickedRequest.getRequest(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                DataForRequests myDataList = (DataForRequests) bundle.getSerializable("one request");

                if (myDataList != null)
                    listOfRequestsForRequests.add(myDataList);
                addapterViewForRequests.setRequest(listOfRequestsForRequests);


            }

        }
    }
}
