package udacity.com.argondy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class HomeActivity extends AppCompatActivity {

    Button requestBtn, myeventsBtn, allEventsBtn, dayEventsBtn;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_notification:
                intent = new Intent(HomeActivity.this, NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.action_search:
                intent = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        requestBtn = (Button) findViewById(R.id.myRequestsBtn);
        myeventsBtn = (Button) findViewById(R.id.MyEventBtn);
        allEventsBtn = (Button) findViewById(R.id.AllEventsBtn);
        dayEventsBtn = (Button) findViewById(R.id.DayEventsBtn);





        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RequestsActivity.class);
                startActivity(intent);
            }
        });
        myeventsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MyEventsActivity.class);
                startActivity(intent);
            }
        });
        allEventsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, AllEventsActivity.class);
                startActivity(intent);
            }
        });
        dayEventsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, DayEventsActivity.class);
                startActivity(intent);
            }
        });


    }
}
