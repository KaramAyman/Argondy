package udacity.com.argondy;

import android.os.Parcel;
import android.os.Parcelable;

public class DataForEvent implements Parcelable {

    public   String description, name;
    public   Integer imageUrl;

    DataForEvent(){}
    protected DataForEvent(Parcel in) {
        description = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            imageUrl = null;
        } else {
            imageUrl = in.readInt();
        }
    }

    public static final Creator<DataForEvent> CREATOR = new Creator<DataForEvent>() {
        @Override
        public DataForEvent createFromParcel(Parcel in) {
            return new DataForEvent(in);
        }

        @Override
        public DataForEvent[] newArray(int size) {
            return new DataForEvent[size];
        }
    };

    public String getdescription() {
        return description;
    }

    public Integer getimage() {
        return imageUrl;
    }

    public String getname() {
        return name;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    public void setimage(Integer image) {
        this.imageUrl = image;
    }

    public void setname(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
        dest.writeString(name);
        if (imageUrl == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(imageUrl);
        }
    }
}
