package udacity.com.argondy;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class AddapterViewForRequests extends RecyclerView.Adapter<AddapterViewForRequests.ForecastAdapterViewHolder> {

    private List<DataForRequests> requestThatWillViewed;
    Context mContext;
    private final AddapterViewForRequests.AddapterOnClickListener mClickListener;


    public interface AddapterOnClickListener {
        void OnClick(DataForRequests mClickedRequest);
    }

    public AddapterViewForRequests(Context context, AddapterOnClickListener clickListener) {
        requestThatWillViewed = new ArrayList<>();
        mContext = context;
        mClickListener =  clickListener;
    }

    public class ForecastAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView request;

        public ForecastAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            request = itemView.findViewById(R.id.requestitem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int addapterPosition = getAdapterPosition();
                    DataForRequests ClickedName = requestThatWillViewed.get(addapterPosition);
                    mClickListener.OnClick(ClickedName);
                }
            });
        }
    }


    public void setRequest(List<DataForRequests> mRequestThatWillViewed) {
        requestThatWillViewed.clear();
        requestThatWillViewed.addAll(mRequestThatWillViewed);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AddapterViewForRequests.ForecastAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int layoutIdForBakingItem = R.layout.requests_item;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(layoutIdForBakingItem, viewGroup, false);
        return new AddapterViewForRequests.ForecastAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddapterViewForRequests.ForecastAdapterViewHolder forecastAdapterViewHolder, int i) {
        // You Use This To Full The RecyclerView Items
        String mRequest = requestThatWillViewed.get(i).getRequest();
        forecastAdapterViewHolder.request.setText(mRequest);
    }
    @Override
    public int getItemCount() {
        return requestThatWillViewed.size();
    }


}
